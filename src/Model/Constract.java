/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author thanh
 */
public class Constract {

    private int duration;
    private double income;
    private double interestrate;
    private double lend;
    private int age;
    private double mortgage;

    public Constract() {
    }

    public Constract(int duration, double income, double interestrate, double lend, int age,  double mortgage) {
        this.duration = duration;
        this.income = income;
        this.interestrate = interestrate;
        this.lend = lend;
        this.age = age;
        this.mortgage = mortgage;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public double getIncome() {
        return income;
    }

    public void setIncome(double income) {
        this.income = income;
    }


    public double getInterestrate() {
        return interestrate;
    }

    public void setInterestrate(double interestrate) {
        this.interestrate = interestrate;
    }

    public double getLend() {
        return lend;
    }

    public void setLend(double lend) {
        this.lend = lend;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }


    public double getMortgage() {
        return mortgage;
    }

    public void setMortgage(double mortgage) {
        this.mortgage = mortgage;
    }

   
    
}
