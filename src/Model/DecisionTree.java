/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import weka.classifiers.trees.J48;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;

/**
 *
 * @author thanh
 */
public class DecisionTree {

    public Instances trainingData;

    public DecisionTree(String fileName) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            trainingData = new Instances(reader);
            trainingData.setClassIndex(trainingData.numAttributes() - 1);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public J48 performTraining() {
        J48 j48 = new J48();
        String[] options = {"-U"};
//        Use unpruned tree. -U
        try {
            j48.setOptions(options);
            j48.buildClassifier(trainingData);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return j48;
    }

    public Instance getTestInstance(
            String binding, String multicolor, String genre) {
        Instance instance = new DenseInstance(3);
        instance.setDataset(trainingData);
        instance.setValue(trainingData.attribute(0), binding);
        instance.setValue(trainingData.attribute(1), multicolor);
        instance.setValue(trainingData.attribute(2), genre);
        return instance;
    }

    public Instance getTestInstance(double a, double b, double c, double d) {
        Instance instance = new DenseInstance(4);
        instance.setDataset(trainingData);
        instance.setValue(trainingData.attribute(0), a);
        instance.setValue(trainingData.attribute(1), b);
        instance.setValue(trainingData.attribute(2), c);
        instance.setValue(trainingData.attribute(3), d);

        return instance;
    }

    public Instance getTestInstance(Instances trainingData, Constract constract) {
        Instance instance = new DenseInstance(6);
        instance.setDataset(trainingData);
        instance.setValue(trainingData.attribute(0), constract.getDuration());
        instance.setValue(trainingData.attribute(1), constract.getIncome());
        instance.setValue(trainingData.attribute(2), constract.getInterestrate());
        instance.setValue(trainingData.attribute(3), constract.getLend());
        instance.setValue(trainingData.attribute(4), constract.getAge());
        instance.setValue(trainingData.attribute(5), constract.getMortgage());

        return instance;
    }
     public Instance getTestInstance(Constract constract) {
        Instance instance = new DenseInstance(6);
        instance.setDataset(trainingData);
        instance.setValue(trainingData.attribute(0), constract.getDuration());
        instance.setValue(trainingData.attribute(1), constract.getIncome());
        instance.setValue(trainingData.attribute(2), constract.getInterestrate());
        instance.setValue(trainingData.attribute(3), constract.getLend());
        instance.setValue(trainingData.attribute(4), constract.getAge());
        instance.setValue(trainingData.attribute(5), constract.getMortgage());

        return instance;
    }
    //public Instance getTestInstance(2,4.5,5.8,9,0,35,"ret_allw",9,0,"yes",11,"below_average",9,"full",1,"full")

}
