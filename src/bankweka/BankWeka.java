/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankweka;

import Controller.TreeController;
import Model.Constract;
import Model.DecisionTree;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import weka.classifiers.trees.J48;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

/**
 *
 * @author thanh
 */
public class BankWeka {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            // TODO code application logic here
            
            //DecisionTree decisionTree = new DecisionTree("credit3.arff");
            

            DecisionTree decisionTree1 = new DecisionTree("credit1.arff");
            

            //System.out.println(tree.toString());
//            Instance testInstance = decisionTree.
//                    getTestInstance("Leather", "yes", "historical");
//            Instance testInstance = decisionTree.
//                    getTestInstance(5.9, 3.0, 5.1, 1.9);
//            int result = (int) tree.classifyInstance(testInstance);
//            String results = decisionTree.trainingData.attribute(4).value(result);
//            System.out.println(
//                    "Test with: " + testInstance + "  Result: " + results);
            Scanner sc = new Scanner(System.in);
            System.out.print("Nhập thời gian vay: ");
            int diuration = sc.nextInt();
            System.out.print("Nhập thu nhập khách hang: ");
            double income = sc.nextDouble();
            System.out.print("Nhập lãi suât: ");
            double interest = sc.nextDouble();
            System.out.print("Nhập số tiền vay: ");
            double lend = sc.nextFloat();
            System.out.print("Nhập tuổi khách hang: ");
            int age = sc.nextInt();
            System.out.print("Nhập giá trị tài sản thế chấp: ");
            double mortgage = sc.nextDouble();

            DecisionTree decisionTree = new DecisionTree("credit3.arff");
            Constract con = new Constract(diuration, income, interest, lend, age, mortgage);
            Instance testInstance = decisionTree.
                    getTestInstance(con);
            J48 tree = decisionTree.performTraining();
            int guaranteed = (int) tree.classifyInstance(testInstance);
            String resultsguaranteed = decisionTree.trainingData.attribute(6).value(guaranteed);
            
            decisionTree = new DecisionTree("credit2.arff");
            testInstance = decisionTree.
                    getTestInstance(con);
            tree = decisionTree.performTraining();
            int term = (int) tree.classifyInstance(testInstance);
            String resultTerm = decisionTree.trainingData.attribute(6).value(term);
            
            decisionTree = new DecisionTree("credit1.arff");
            testInstance = decisionTree.
                    getTestInstance(con);
            tree = decisionTree.performTraining();
            int purpose = (int) tree.classifyInstance(testInstance);
            String resultspurpose = decisionTree.trainingData.attribute(6).value(purpose);
            
            System.out.println("Loại hợp đồng: " + resultspurpose + " "+ resultsguaranteed + " "+resultTerm);
            
//        TreeController tc = new TreeController();
//        J48 tree = tc.CreateTree("E:\\\\Program\\\\Weka-3-8-4\\\\data\\\\credit-g.arff");
//        
//        System.out.println(tree);
            return;
        } catch (Exception ex) {
            Logger.getLogger(BankWeka.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
