/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import weka.classifiers.trees.J48;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;

/**
 *
 * @author thanh
 */
public class TreeController {
    public J48 CreateTree(String path){
        try {
            ConverterUtils.DataSource src = new ConverterUtils.DataSource(path);
            Instances dt = src.getDataSet();
            dt.setClassIndex(dt.numAttributes() - 1);

            String[] options = new String[4];
            options[0] = "-C";
            options[1] = "0.1";
            options[2] = "-M";
            options[3] = "2";
            J48 mytree = new J48();
            mytree.setOptions(options);
            mytree.buildClassifier(dt);
             System.out.println(mytree.getTechnicalInformation());
             return mytree;
            //weka.core.SerializationHelper.write("E:\\Dat\\IT\\myDT.model.txt", mytree);
        }
        catch (Exception e) {
            System.out.println("Error!!!!\n" + e.getMessage());
            return null;
        }
    }
}
